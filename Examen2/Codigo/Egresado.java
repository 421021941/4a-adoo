public class Egresado {
    public String nombre;
    public String apellPaterno;
    public String apellMaterno;
    public String correo;
    public String generacion;
    public boolean activLaboral;
    public String tipoInstitucion;
    public Carrera carrera;
    public int matricula;
    
    public Egresado(String nombre, String apellPaterno, String apellMaterno, String correo, String generacion,
            boolean activLaboral, String tipoInstitucion, Carrera carrera, int matricula) {
        this.nombre = nombre;
        this.apellPaterno = apellPaterno;
        this.apellMaterno = apellMaterno;
        this.correo = correo;
        this.generacion = generacion;
        this.activLaboral = activLaboral;
        this.tipoInstitucion = tipoInstitucion;
        this.carrera = carrera;
        this.matricula = matricula;
    }

    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getApellPaterno() {
        return apellPaterno;
    }
    public void setApellPaterno(String apellPaterno) {
        this.apellPaterno = apellPaterno;
    }
    public String getApellMaterno() {
        return apellMaterno;
    }
    public void setApellMaterno(String apellMaterno) {
        this.apellMaterno = apellMaterno;
    }
    public String getCorreo() {
        return correo;
    }
    public void setCorreo(String correo) {
        this.correo = correo;
    }
    public String getGeneracion() {
        return generacion;
    }
    public void setGeneracion(String generacion) {
        this.generacion = generacion;
    }
    public boolean getActivLaboral() {
        return activLaboral;
    }
    public void setActivLaboral(boolean activLaboral) {
        this.activLaboral = activLaboral;
    }
    public String getTipoInstitucion() {
        return tipoInstitucion;
    }
    public void setTipoInstitucion(String tipoInstitucion) {
        this.tipoInstitucion = tipoInstitucion;
    }
    public Carrera getCarrera() {
        return carrera;
    }
    public void setCarrera(Carrera carrera) {
        this.carrera = carrera;
    }
    public int getMatricula() {
        return matricula;
    }
    public void setMatricula(int matricula) {
        this.matricula = matricula;
    }

    @Override
    public String toString() {
        return "Egresado [ "+nombre+" "+apellPaterno+" "+apellMaterno+", correo: "+correo
                +", generacion:"+generacion+", carrera="+carrera+", matricula="+matricula
                +", Actividad Laboral="+activLaboral+", Tipo Institucion="+tipoInstitucion+"]";
    }
}
