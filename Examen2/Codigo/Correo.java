public class Correo {
    public static String correo;
    public String nombre;
    public Carrera carrera;
    
    public Correo(String correo, String nombre, Carrera carrera) {
        this.correo = correo;
        this.nombre = nombre;
        this.carrera = carrera;
    }
    public static void enviar_correo(Egresado e,String correo){
        System.out.println("Correo enviado a la direccion:"+correo);
    }
    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Carrera getCarrera() {
        return carrera;
    }

    public void setCarrera(Carrera carrera) {
        this.carrera = carrera;
    }
}
