public class Registro {
    public static void main(String[] args) {
        Egresado[] egresados ={ new Egresado("Alvaro","Alvarado","Alvarez","alval@gmail.com","2001-2006",false,"N/A",Carrera.INGCOMPUTACION,12333),
        new Egresado("Braulio", "Briseño","Briones","brbrisbr@gmail.com","2003-2007",true,"Gubernamental",Carrera.INGCOMPUTACION,11892),
        new Egresado("Carlos","Cortes","Cruz","CCCruz@gmail.com","2003-2007",true,"Empresaprivada",Carrera.INGELECTRICISTA,86483),};
        for (Egresado egresado : egresados) {
            Correo.enviar_correo(egresado, egresado.correo);
            Reporte.genReporte(egresado);
        }
        Reporte.mostrarreporte();
    }
}
