public enum Carrera {
    INGCOMPUTACION("Ingenieria en computacion"),
    INGSOFTWARE("Ingenieria de Software"),
    INGELECTRICISTA("Ingenieria en Electricista"),
    INGEELICINDUSTRIAL("Ingenieria en Electronica Industrial"),
    INGDISINDUSTRIAL("Ingenieria en Diseño Industrial"),
    INGROBMECA("Ingenieria en Robotica y Mecatronica");
    public String descripcion;
   
    private Carrera(String descripcion){
        this.descripcion=descripcion;
    }
    public String getDescripcion() {
        return descripcion;
    }
}
