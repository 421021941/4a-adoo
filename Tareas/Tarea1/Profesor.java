public class Profesor {
	private String rfc;
	private String nombre;
	private String apaterno;
	private String amaterno;

	public Profesor(String rfc, String nombre, String apaterno, String amaterno) {
		this.rfc = rfc;
		this.nombre = nombre;
		this.apaterno = apaterno;
		this.amaterno = amaterno;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApaterno() {
		return apaterno;
	}

	public void setApaterno(String apaterno) {
		this.apaterno = apaterno;
	}

	public String getAmaterno() {
		return amaterno;
	}

	public void setAmaterno(String amaterno) {
		this.amaterno = amaterno;
	}
}