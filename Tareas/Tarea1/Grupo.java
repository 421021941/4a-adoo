import java.util.List;

public class Grupo {
	private int clave;
	private String nombre;
	private String semestre;
	private int grado;
	private List<Alumno> alumnos;
	private List<Materia> materias;
	private String calificacion;

	public Grupo(int clave, String nombre, String semestre, int grado) {
		this.clave = clave;
		this.nombre = nombre;
		this.semestre = semestre;
		this.grado = grado;
	}

	public int getClave() {
		return clave;
	}

	public void setClave(int clave) {
		this.clave = clave;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getSemestre() {
		return semestre;
	}

	public void setSemestre(String semestre) {
		this.semestre = semestre;
	}

	public int getGrado() {
		return grado;
	}

	public void setGrado(int grado) {
		this.grado = grado;
	}

	public void AddAlumnos(Alumno alumno){
		alumnos.add(alumno);
	}

	public void AddMateria(Materia materia){
		materias.add(materia);
	}

	public void AplicaExamen(Alumno alumno, Examen examen, int aciertos){
		if (examen.isActivo()){
			float numAciertos = examen.getNoAciertos();
			float calif = (numAciertos * 100) / aciertos;
			calificacion = "Alumno "+alumno.getMatricula() +" obtuvo "+calif+" de calificación en el examen "+examen.getNombre();
		}
	}
}