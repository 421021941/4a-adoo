public class Examen {
	private int clave;
	private String nombre;
	private int noAciertos;
	private boolean activo;

	public Examen(int clave, String nombre, int noAciertos, boolean activo) {
		this.clave = clave;
		this.nombre = nombre;
		this.noAciertos = noAciertos;
		this.activo = activo;
	}

	public int getClave() {
		return clave;
	}

	public void setClave(int clave) {
		this.clave = clave;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getNoAciertos() {
		return noAciertos;
	}

	public void setNoAciertos(int noAciertos) {
		this.noAciertos = noAciertos;
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}
}