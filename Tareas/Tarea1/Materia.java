public class Materia {
	private int clave;
	private String nombre;
	private String descripcion;
	private int creditos;
	private Profesor profesor;

	public Materia(int clave, String nombre, String descripcion, int creditos) {
		this.clave = clave;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.creditos = creditos;
	}

	public int getClave() {
		return clave;
	}

	public void setClave(int clave) {
		this.clave = clave;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getCreditos() {
		return creditos;
	}

	public void setCreditos(int creditos) {
		this.creditos = creditos;
	}

	public Profesor getProfesor() {
		return profesor;
	}

	public void setProfesor(Profesor profesor) {
		this.profesor = profesor;
	}

	public void AddProfesor(Profesor profe){
		profesor = profe;
	}
}